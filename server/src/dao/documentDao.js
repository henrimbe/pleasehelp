// @flow

const Dao = require("./dao.js");

export class DocumentDAO extends Dao {
    constructor(pool) {
        super(pool);
    }

    /**
     * Fetches a document by id
     * @param document_id
     * @param callback
     */
    getDocumentById(document_id: number, callback: (status: string, data: string) => void) {
        let values = [document_id];
        super.query("CALL get_document_by_id(?)",
            values,
            callback);
    }

    /**
     * Fetches documents by event_id
     * @param event_id
     * @param callback
     */
    getDocumentByEvent(event_id: number, callback: (status: string, data: string) => void) {
        let values = [event_id];
        super.query("CALL get_document_by_event(?)",
            values,
            callback);
    }

    /**
     * Inserts documents
     * @param name
     * @param file
     * @param event_id
     * @param callback
     */
    insertDocument(name: string, file: Blob, event_id: number, callback: (status: string, data: string) => void) {
        let values = [name, file, event_id];
        console.log("file:");
        console.log(file);

        super.query("CALL insert_document(?,?,?)",
            values,
            callback);
    }
}
