const express = require("express");

const documentController = require("../controllers/document");

const router = express.Router();

router.get("/", documentController.getDocumentByQuery);
router.post("/", documentController.insertDocument);

module.exports = router;
