//@flow

import { DocumentDAO } from "../dao/documentDao.js";
const pool = require("../server.js");

const documentDao = new DocumentDAO(pool);

// Get document by id or all documents by event_id
exports.getDocumentByQuery = (req, res, next) => {
    console.log(`Got request from client: GET /document`);
    if (req.query.document) {
        documentDao.getDocumentById(req.query.document, (err, rows) => {
            console.log(rows);
            res.json(rows);
        })
    } else if (req.query.event) {
        documentDao.getDocumentByEvent(req.query.event, (err, rows) => {
            console.log(rows);
            res.json(rows);
        })
    }
};

// Insert document
exports.insertDocument = (req, res, next) => {
    console.log(`Got request from client: POST /api/document`);
    console.log(req.body.file);
    console.log("test");

    documentDao.insertDocument(req.body.name, req.body.file, req.query.event,(err, rows) => {
        res.send(rows);
    });
};
