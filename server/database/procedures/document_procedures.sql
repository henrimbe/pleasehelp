/**
  Delete all procedures for recreation
 */
DROP PROCEDURE IF EXISTS get_document_by_id;
DROP PROCEDURE IF EXISTS get_document_by_event;
DROP PROCEDURE IF EXISTS insert_document;

/**
  Fetches document based on an id

  IN document_id_in: Id of the document

  Issued by: getDocumentById(documentId: number)
 */
CREATE PROCEDURE get_document_by_id(IN document_id_in INT)
BEGIN
  SELECT * FROM document WHERE document_id=document_id_in;
END;

/**
  Fetches documents based on an event

  IN document_event_in: Id of the event

  Issued by: getDocumentByEvent(eventId: number)
 */
CREATE PROCEDURE get_document_by_event(IN document_event_in INT)
BEGIN
  SELECT * FROM document WHERE event=document_event_in;
END;

/**
  Inserts a new document

  IN name_in: Name of the document

  IN file_in: File

  IN event_in: Id of event

  Issued by: insertDocument(name: string, file: Blob, event: number)
 */
CREATE PROCEDURE insert_document(IN name_in VARCHAR(50), IN file_in BLOB, IN event_id_in INT(11))
BEGIN
  INSERT INTO document (name, file, event)
  VALUES (name_in, file_in, event_id_in);
END;