import * as React from 'react';
import {Component} from "react-simplified";
import { createHashHistory } from 'history';
const history = createHashHistory();
import { Document, documentService } from "../services/documentService";

export class DocumentMain extends Component <{match: {params: {event: number}}}> {
    form: any = null;
    name: string = "";
    files: Blob[] = [];
    errorMessage: string = "";
    documentList: Document[] = [];
    testFile: Blub = null;

    render() {
        return(
            <div className="row justify-content-center">
                <div className="row" style={{}}>
                    <div className="card" style={{}}>
                        <form ref={e => (this.form = e)}>
                            <input
                                type="text"
                                className="form-control"
                                value={this.name}
                                placeholder="Filnavn"
                                onChange={(event: SyntheticInputEvent<HTMLInputElement>) => (this.name = event.target.value)}
                                required
                                maxLength={50}
                            />
                            <input
                                type="file"
                                className="form-control"
                                value={this.file}
                                placeholder="illegal music"
//                              onChange={(event: SyntheticInputEvent<HTMLInputElement>) => (this.file = event.target.value)}
                                onChange={(e) => this.load(e)}
                                required
                                style={{paddingBottom: "50px", paddingTop: "20px"}}
                            />
                            <button
                                type="button"
                                className="btn btn-dark"
                                style={{}}
                                onClick={this.upload}
                                style={{marginBottom: "0px", marginTop: "20px", width: "100%"}}
                            >Last opp</button>
                        </form>
                        <p style={{color: "red"}}>{this.errorMessage}</p>
                    </div>
                </div>
                <div className="card" style={{width: "25%"}}>
                    <div className="list-group">
                        <li className="list-group-item" style={{}}>
                            <div className="row justify-content-center">
                                Documents with event_id: {this.props.match.params.event}
                            </div>
                        </li>
                        {this.documentList.map(d => (
                            <li onClick={this.download} key={"documentId" + d.document_id} documentId={d.document_id} documentName={d.name} className="list-group-item list-group-item-action">
                                {d.name}
                            </li>
                        ))}
                    </div>
                </div>
            </div>
        )
    }

    mounted() {
        this.fetch();
    }

    load(e: any) {
        console.log(this.files);
        this.files = e.target.files;
        console.log("yo");
        this.testFile = new Blob(new Uint8Array(e.target.result), {type: 'text/plain', name: this.name});
    }

    upload(e: any) {
        let files = this.files;
        let reader = new FileReader();
        console.log(this.testFile);
        /*
        reader.readAsDataURL(files[0]);
        const formData = {file:e.target.result};
        reader.onload=(e)=> {
            console.log("imgdata:" + e.target.result);
        }
        */
        documentService.postDocument(this.props.match.params.event, {name: this.name, file: this.testFile}).then(response => {
            //user response to find out what happened
            this.mounted();
        });
    }

    fetch() {
        documentService.getDocuments(this.props.match.params.event).then(response => {
            //user response to find out what happened
            this.documentList = response;
        });
    }

    download = (event) => {
        console.log("testfile:");
        console.log(this.testFile)
        let saveData = (function () {
            let a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";
            return function (data, fileName) {
                var blob = data,
                    url = window.URL.createObjectURL(blob);
                a.href = url;
                a.download = fileName;
                a.click();
                window.URL.revokeObjectURL(url);
            };
        }());
        saveData(this.testFile, "me");
                    /*
        this.documentList.map(d => {
            if(d.document_id === event.target.getAttribute('documentId')) {

            }
        })
        let saveData = (function () {
            let a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";
            return function (data, fileName) {
                var json = JSON.stringify(data),
                    blob = new Blob([json], {type: "octet/stream"}),
                    url = window.URL.createObjectURL(blob);
                a.href = url;
                a.download = fileName;
                a.click();
                window.URL.revokeObjectURL(url);
            };
        }());
        saveData({ x: 42, s: "hello, world", d: new Date() }, event.target.getAttribute('documentName'));
        */
        //user response to find out what happened
        const reader = new FileReader();
        let blob = event.target.getAttribute('documentFile');
    }

    delete = (event) => {
        documentService.deleteDocument(event.target.getAttribute('documentId')).then(response => {
            //user response to find out what happened
            this.mounted();
        });
    }

    update = (event) => {
        documentService.updateDocument(event.target.getAttribute('documentId'), ).then(response => {
            //user response to find out what happened
            this.mounted();
        });
    }

}
