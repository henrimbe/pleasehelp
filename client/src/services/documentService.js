//@flow

import axios from 'axios';
//import { userService } from "./userService";

export class Document {
    document_id: number;
    name: string;
    file: Blob;
    event: number;

    constructor(name: string, file: Blob) {
        this.name = name;
        this.file = file;
    }
}


export class Contract extends Document {
    artist: number;
}

export class DocumentService {
    getDocument(documentId: number) {
        return axios.get<Document>(`http://localhost:4000/api/document?document=${documentId}`).then(response => response.data);
    }

    getDocumentFile(documentId: number) {
        return axios.get<Blob>(`http://localhost:4000/api/document?file=${documentId}`).then(response => response.data);
    }

    getDocuments(eventId: number) {
        return axios.get<Document[]>(`http://localhost:4000/api/document?event=${eventId}`).then(response => response.data[0]);
    }

    postDocument(eventId: number, data: Object) {
        console.log(data.file);
        return axios.post(`http://localhost:4000/api/document?event=${eventId}`, data).then(response => response.data);
    }

    updateDocument(documentId: number, data: Object) {
        return axios.put(`http://localhost:4000/api/document?document=${documentId}`, data).then(response => response.data);
    }

    deleteDocument(documentId: number) {
        return axios.delete(`http://localhost:4000/api/document?document=${documentId}`).then(response => response.data);
    }

}

export const documentService = new DocumentService();