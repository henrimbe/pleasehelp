// @flow

/**
 * Footer for  all pages
 *
 * @author Victoria Blichfeldt
 */

import React from "react";
import { Component } from "react-simplified";

class Footer extends Component {
    render() {
        return (
            <footer className="text-center">
                <p>&copy; 2020 Team 3</p>
            </footer>
        );
    }
}

export default Footer;